import populations from '../country_population.json.js'

export const getPopulation = (countryCode) => {
    const country = codeTranslations[countryCode] || countryCode
    const population = populations.data.find((element => element.name === country))
    if (!population) console.log(country)
    const pop2020 = (population && population.pop2020) || ''
    return parseInt(pop2020.toString().replace('.', ''), 10) / 10
}

export const uninfectedPopulation = (doc) => {
    const population = getPopulation(doc.country)
    return population - doc.cases
}

export const daysUntilImmunity = (infectionPercentageNecessary, doc) => {
    const population = getPopulation(doc.country)
    const uninfectedPopulation = population - doc.cases
    const needsToBeInfected = uninfectedPopulation / 100 * infectionPercentageNecessary
    const daysUntilImmunity = needsToBeInfected / doc.todayCases
    return daysUntilImmunity
}

const codeTranslations = {
    USA: 'United States',
    UK: 'United Kingdom',
    'S. Korea': 'South Korea',
    Czechia: 'Czech Republic',
    UAE: 'United Arab Emirates',
    'Moldova, Republic of': 'Moldova',
    Bosnia: 'Bosnia and Herzegovina',
    // 'Diamond Princess': '',
    'Réunion': 'Reunion',
    'Côte d\'Ivoire': 'Ivory Coast',
    // 'Diamond Princess': '',
    // 'Channel Islands': '',
    'Palestinian Territory, Occupied': 'Palestine',
    DRC: 'DR Congo',
    Congo: 'Republic of the Congo',
    Macao: 'Macau',
    'Tanzania, United Republic of': 'Tanzania',
    'Libyan Arab Jamahiriya': 'Libya',
    'Syrian Arab Republic': 'Syria',
    'Lao People\'s Democratic Republic': 'Laos',
    'Curaçao': 'Curacao',
    // 'MS Zaandam': '',
    'Cabo Verde': 'Cape Verde',
    'Holy See (Vatican City State)': 'Vatican City',
    'St. Barth': 'Saint Barthélemy',
    'Caribbean Netherlands': '',
    'Falkland Islands (Malvinas)': 'Falkland Islands',
    'Saint Pierre Miquelon': 'Saint Pierre and Miquelon',
}