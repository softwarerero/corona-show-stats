import countries from '../country_covid.json.js'
import { getPopulation, uninfectedPopulation, daysUntilImmunity } from './Population.js'

export const getCountries = (infectionPercentageNecessary) => {
    return countries
        .filter((country) => !removeList.includes(country.country))
        .map((doc) => {
            doc.population = getPopulation(doc.country)
            doc.uninfectedPopulation = uninfectedPopulation(doc)
            doc.needToBeInfected = doc.population / 100 * infectionPercentageNecessary
            doc.timeNeeded = daysUntilImmunity(infectionPercentageNecessary, doc)
            return doc
        })
}

export const getCountryNames = () => {
    // return ['USA', 'Spain']
    return countries
        .filter((country) => !removeList.includes(country.country))
        .map((doc) => doc.country)
}

const removeList = [
    'Diamond Princess',
    'Channel Islands',
    'MS Zaandam',
    'Caribbean Netherlands',
    'Djibouti',
]